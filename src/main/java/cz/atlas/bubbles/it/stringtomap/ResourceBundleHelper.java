package cz.atlas.bubbles.it.stringtomap;

/**
 * Author: it-bubbles.blogspot.com, bubbles.way@gmail.com
 * Date: 20090208
 * $Date$
 * $Revision$
 * $Author$
 */

import javax.swing.*;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * ResourceBundleHelper class to support resource bundel access with StringToMap feature.
 */
public class ResourceBundleHelper {
        /**
         * Resource bundle to be used by this resource manager.
         */
        private static ResourceBundle resourceBundle = null;

        /**
         * Set the resource bundle.
         *
         * @param resBundle the resourceBundle to associate the helper with
         */
        public static void setResourceBundle(final ResourceBundle resBundle) {
                resourceBundle = resBundle;
        }

        /**
         * Get the resource bundle.
         * @return ResourceBundle peviously set or null         
         */
        public static ResourceBundle getResourceBundle() {
                return resourceBundle;
        }

        /**
         * Get string map from specific resource that is contained in specific resource bundle.
         *
         * @param key       the key to sear for
         * @param resBundle the ResourceBundle to search in
         * @return the Map with resource string representation
         */
        public static Map getStringMap(final String key, final ResourceBundle resBundle) {
                Map retVal;
                try {
                        final String stringVal = resBundle.getString(key);
                        retVal = StringToMap.toMap(stringVal.toCharArray());
                } catch (MissingResourceException e) {
                        retVal = new HashMap(); //empty map if resource not found
                }
                return retVal;
        }

        /**
         * Get string map from specific resource.
         * Resource must be contained in resource bundle of associated with  Resource Mananger.
         *
         * @param key the key to search for
         * @return the Map with resource string representation
         */
        public static Map getStringMap(final String key) {
                Map retVal;
                if (resourceBundle != null) {
                        retVal = getStringMap(key, resourceBundle);
                } else {
                        retVal = new HashMap(); //empty map if resource not found
                }
                return retVal;
        }

        /**
         * Helper method to load ImageIcon from file.
         *
         * @param iconFile the full path found in classpath to image file
         * @return ImageIcon for given path or null if path is not found
         */
        public static ImageIcon getIcon(final String iconFile) {
                final URL url = Thread.currentThread().getContextClassLoader().getResource(iconFile);
                return url != null ? new ImageIcon(url) : null;                
        }
}
