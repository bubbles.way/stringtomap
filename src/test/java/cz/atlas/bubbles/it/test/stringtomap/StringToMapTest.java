package cz.atlas.bubbles.it.test.stringtomap;

import cz.atlas.bubbles.it.stringtomap.StringToMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Map;

/**
 * Author: it-bubbles.blogspot.com, bubbles.way@gmail.com
 * Date: 20090105
 * $Date$
 * $Revision$
 * $Author$
 */

/**
 * Basic TestNG unit tests for stringtomap
 */
  @Test(groups = {"basic"})
public class StringToMapTest {
        private static final char KEY_SEP = ':';
        private static final char ITEM_SEP = '|';
        private static final char ESCAPE = '&';
        private static final String KEY = "text";
        private static final String VALUE = "text value";
        private static final String SIMPLE_VALUE = "simple string";

        public void mapSimpleValueTest() {
                LOGGER.info("==> mapSimpleValueTest");
                simpleValueHelper(SIMPLE_VALUE);
                LOGGER.info("<== mapSimpleValueTest");
        }

        public void mapSimpleValueEscapeTest() {
                LOGGER.info("==> mapSimpleValueEscapeTest");
                final String testString = SIMPLE_VALUE + ESCAPE + KEY_SEP + SIMPLE_VALUE + ESCAPE + ITEM_SEP + SIMPLE_VALUE;
                simpleValueHelper(testString);
                LOGGER.info("<== mapSimpleValueEscapeTest");
        }

        private static void simpleValueHelper(String testString) {
                LOGGER.info("==> simpleValueHelper testString: {}", testString);
                Map map = mapHelper(testString, 1);
                Assert.assertEquals(map.keySet().toArray().length, 1);
                Assert.assertEquals(map.keySet().toArray()[0], StringToMap.MAIN_KEY);
                Assert.assertEquals(map.values().toArray().length, 1);
                Assert.assertEquals(map.values().toArray()[0], testString);
                LOGGER.info("<== simpleValueHelper testString");
        }

        public void mapValueTest() {
                LOGGER.info("==> mapSimpleValueTest");
                final String testString = KEY + KEY_SEP + VALUE;
                valueHelper(KEY, VALUE, testString);
                LOGGER.info("<== mapSimpleValueTest");
        }

        public void mapValueEscapeTest() {
                LOGGER.info("==> mapSimpleValueTest");
                final String testStringKey = KEY + ESCAPE + KEY_SEP + KEY;
                final String testStringValue = VALUE + ESCAPE + KEY_SEP + VALUE + ESCAPE + ITEM_SEP + VALUE;
                final String testString = testStringKey + KEY_SEP + testStringValue;
                valueHelper(testStringKey, testStringValue, testString);
                LOGGER.info("<== mapSimpleValueTest");
        }

        public void defaultSeparatorsTest() {
                LOGGER.info("==> defaultSeparatorsTest");
                final String testStringKey = KEY + ESCAPE + KEY_SEP + KEY;
                final String testStringValue = VALUE + ESCAPE + KEY_SEP + VALUE + ESCAPE + ITEM_SEP + VALUE;
                final String testString = testStringKey + KEY_SEP + testStringValue;
                Map map = StringToMap.toMap(testString.toCharArray(), ITEM_SEP, KEY_SEP, ESCAPE);
                Map mapDefault = StringToMap.toMap(testString.toCharArray());
                for (Object key : map.keySet()) {
                        Assert.assertTrue(mapDefault.keySet().contains(key));
                        Assert.assertEquals(map.get(key), mapDefault.get(key));
                }
                LOGGER.info("<== defaultSeparatorsTest");
        }

        public void separatorAtEndTest() {
                LOGGER.trace("==> separatorAtEndTest");
                // handle ITEM separator eat the end
                String testString = KEY + KEY_SEP + VALUE + ITEM_SEP; //last item separator is ignored
                valueHelper(KEY, VALUE, testString);
                testString = KEY + KEY_SEP + VALUE + KEY_SEP; //key separator at end is added to value
                valueHelper(KEY, VALUE + KEY_SEP, testString);
                LOGGER.trace("<== separatorAtEndTest");
        }

        public void mapSimpleValueAndValueTest() {
                LOGGER.info("==> mapSimpleValueAndValueTest");
                final String testString = SIMPLE_VALUE + ITEM_SEP + KEY + KEY_SEP + VALUE;
                simpleValueAndValueHelper(SIMPLE_VALUE, KEY, VALUE, testString);
                LOGGER.info("<== mapSimpleValueAndValueTest");
        }

        public void mapSimpleValueAndValueEscapeTest() {
                LOGGER.info("==> mapSimpleValueAndValueTest");
                final String simpleString = SIMPLE_VALUE + ESCAPE + KEY_SEP + ESCAPE + ITEM_SEP;
                final String key = KEY + ESCAPE + KEY_SEP + ESCAPE + ITEM_SEP;
                final String value = VALUE + ESCAPE + KEY_SEP + ESCAPE + ITEM_SEP + VALUE;
                final String testString = simpleString + ITEM_SEP + key + KEY_SEP + value;
                simpleValueAndValueHelper(simpleString, key, value, testString);
                LOGGER.info("<== mapSimpleValueAndValueTest");
        }

        private static void valueHelper(String testStringKey, String testStringValue, String testString) {
                LOGGER.trace("==> valueHelper  testStringKey:{} testStringValue:{} testString:{}",
                        new String[]{testStringKey, testStringValue, testString});
                Map map = mapHelper(testString, 1);
                Assert.assertEquals(map.keySet().toArray()[0], testStringKey);
                Assert.assertEquals(map.values().toArray()[0], testStringValue);
                LOGGER.trace("<== valueHelper");
        }


        private static void simpleValueAndValueHelper(String simpleString, String key, String value, String testString) {
                LOGGER.trace("==> simpleValueAndValueHelper simpleString:{} key:{} value:{} testString:{}",
                        new String[]{simpleString, key, value, testString});
                Map map = mapHelper(testString, 2);
                Assert.assertEquals(map.get(StringToMap.MAIN_KEY), simpleString);
                Assert.assertEquals(map.get(key), value);
                LOGGER.trace("<== simpleValueAndValueHelper");
        }

        private static Map mapHelper(String testString, int expectedSize) {
                LOGGER.trace("==> mapHelper testString: {} expectedSize: {}", testString, expectedSize);
                Map map = StringToMap.toMap(testString.toCharArray(), ITEM_SEP, KEY_SEP, ESCAPE);
                Assert.assertNotNull(map);
                Assert.assertEquals(map.size(), expectedSize);
                LOGGER.trace("<== mapHelper map: {}", map);
                return map;
        }

        /**
         * Initialize logging.
         */
        private static final Logger LOGGER = LoggerFactory.getLogger(StringToMapTest.class);
}
