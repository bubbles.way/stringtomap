package cz.atlas.bubbles.it.test.stringtomap;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ResourceBundle;
import java.util.Map;

import cz.atlas.bubbles.it.stringtomap.ResourceBundleHelper;

/**
 * Author: it-bubbles.blogspot.com, bubbles.way@gmail.com
 * Date: 20090210
 * SVN $Date$
 * SVN $Revision$
 * SVN $Author$
 */
@Test(groups = {"resource"})
public class ResourceBundleHelperTest {

        private static final String NEWDB = "gui.action.file.newdb";
        private static final String OPENDB = "gui.action.file.opendb";
        private static final String REOPENDB = "gui.menu.file.reopendb";
        private static final String CLOSEDB = "gui.action.file.closedb";
        private static final String OPTIONS = "gui.action.file.options";
        private static ResourceBundle resBundle;

        @BeforeClass
        public void initResourceBundle() {
                LOGGER.info("==> initResourceBundle");
                resBundle = ResourceBundle.getBundle("cz.atlas.bubbles.it.test.stringtomap.resources");
                ResourceBundleHelper.setResourceBundle(resBundle);
                LOGGER.info("<== initResourceBundle resBundle: {}", resBundle);
        }

        public void resourceBundleSetting() {
                LOGGER.info("<== resourceBundleSetting");
                ResourceBundle bundle = ResourceBundleHelper.getResourceBundle();
                Assert.assertNotNull(bundle);
                Assert.assertEquals(resBundle, bundle);
                ResourceBundleHelper.setResourceBundle(null);
                bundle = ResourceBundleHelper.getResourceBundle();
                Assert.assertNull(bundle);
                ResourceBundleHelper.setResourceBundle(resBundle);
                bundle = ResourceBundleHelper.getResourceBundle();
                Assert.assertNotNull(bundle);
                Assert.assertEquals(resBundle, bundle);
                LOGGER.info("==> resourceBundleSetting");

        }

        public void readValueFromResources() {
                LOGGER.info("==> readValueFromResources");
                Map resMap = ResourceBundleHelper.getStringMap(NEWDB);
                Assert.assertEquals(resMap.size(), 4);
                Assert.assertEquals(resMap.get("lb"), "New database...");
                Assert.assertEquals(resMap.get("mn"), "N");
                Assert.assertEquals(resMap.get("ic"), "new.png");
                Assert.assertEquals(resMap.get("tp"), "Create new empty database in specific location (directory).");
                Map resMap2 = ResourceBundleHelper.getStringMap(NEWDB, resBundle);
                Assert.assertEquals(resMap2, resMap);
                resMap2 = ResourceBundleHelper.getStringMap(OPENDB);
                Assert.assertFalse(resMap2.equals(resMap));
                Assert.assertEquals(ResourceBundleHelper.getStringMap(NEWDB),
                    ResourceBundleHelper.getStringMap(NEWDB, resBundle));
                Assert.assertEquals(ResourceBundleHelper.getStringMap(OPENDB),
                    ResourceBundleHelper.getStringMap(OPENDB, resBundle));
                Assert.assertEquals(ResourceBundleHelper.getStringMap(REOPENDB),
                    ResourceBundleHelper.getStringMap(REOPENDB, resBundle));
                Assert.assertEquals(ResourceBundleHelper.getStringMap(CLOSEDB),
                    ResourceBundleHelper.getStringMap(CLOSEDB, resBundle));
                Assert.assertEquals(ResourceBundleHelper.getStringMap(OPTIONS),
                    ResourceBundleHelper.getStringMap(OPTIONS, resBundle));
                LOGGER.info("<== readValueFromResources");
        }

        /**
         * Initialize logging.
         */
        private static final Logger LOGGER = LoggerFactory.getLogger(ResourceBundleHelperTest.class);
}
